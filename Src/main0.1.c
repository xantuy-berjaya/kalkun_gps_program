/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ssd1306_adafruit.h"
#include "adafruit_gfx.h"
#include "minmea.h"

#define POWER_ON		GPIO_PIN_0
#define BTT_POWER_OFF	GPIO_PIN_1

#define BUTTON_OK 		GPIO_PIN_10
#define BUTTON_CLC 		GPIO_PIN_11

#define BATT_MON_EN		GPIO_PIN_7

//BUTTON STATE READ
#define BTT_ETR_TRUE	HAL_GPIO_ReadPin(GPIOA, BUTTON_OK) == GPIO_PIN_RESET
#define BTT_CLC_TRUE	HAL_GPIO_ReadPin(GPIOA, BUTTON_CLC) == GPIO_PIN_RESET
#define BTT_PWR_TRUE	HAL_GPIO_ReadPin(GPIOB, BTT_POWER_OFF) == GPIO_PIN_SET

//OUTPUT STATE
#define POWER_ON_DVC	HAL_GPIO_WritePin(GPIOB, POWER_ON, GPIO_PIN_SET)
#define POWER_OFF_DVC	HAL_GPIO_WritePin(GPIOB, POWER_ON, GPIO_PIN_RESET)

#define EN_BTT_MON		HAL_GPIO_WritePin(GPIOA, BATT_MON_EN, GPIO_PIN_SET)
#define DS_BTT_MON		HAL_GPIO_WritePin(GPIOA, BATT_MON_EN, GPIO_PIN_RESET)

//GPIOC
#define LED_ERR			GPIO_PIN_8
#define LED_STATUS		GPIO_PIN_9
#define BZZ_MODE		GPIO_PIN_15

#define LD_STS_ON		HAL_GPIO_WritePin(GPIOA, LED_ERR, GPIO_PIN_SET)
#define LD_STS_OFF		HAL_GPIO_WritePin(GPIOA, LED_ERR, GPIO_PIN_RESET)
#define LD_IDL_ON		HAL_GPIO_WritePin(GPIOA, LED_STATUS, GPIO_PIN_SET)
#define LD_IDL_OFF		HAL_GPIO_WritePin(GPIOA, LED_STATUS, GPIO_PIN_RESET)


//#define ADC_BUF_LEN 	1024
#define ADC_BUF_LEN 	4096
#define ADC_VREFF		3.269
#define VRESS_PLL		2
#define ADC1_CH6MAX		2860
#define ADC1_CH6MIN		3440

#define BATT_MIN_PRC	3.62	//1.19	//2262	// 3.62 Volt
#define BATT_MAX_PRC	4.05	//1.40	//2568	// 4.21 Volt
#define MAX_PRCENTAGE	100
#define MIN_PRCENTAGE	0

#define UTC_IND_CLK		7			// UTC+7
#define	SEC_1HOUR		3600		// Second

//#define RUN_DBG_USBCOMM
//#define	RUN_DBG_SERIAL

//#define POWER_BUTTON_EN


//***** Function ********

char 	ch_dpy_buffer1			[128];
char	gps_rawdata				[128];
char 	ch_dpy_bufferGPS		[2];
char 	compare_gpsdata[10][32];

int 	idx = 0, huart1_idx = 0, idxgp = 0,	flag = 0;

float 	heading = 0;
float 	fl_rawtovadc, fl_finevadc, fl_rawdataadc, batt_meter;	//
float 	tempadc;

unsigned long ul_epochnih;

bool	gps_validity;
bool	Charging_flag;
bool	tick_tim2_val;

uint8_t	count_pwr_off;
uint8_t u8_gpsERdata			[7];
uint8_t u8_gpsEWdata			[7];
uint8_t eeprom_last_addr		[2];

uint16_t tick_tim2, tick_me;

uint8_t u16_tc_parking, u8_tc_lw_spd, u8_tc_stb_spd;

uint16_t	u16_tmp_eep_wraddr;
uint16_t	u16_bttlvsts;

__IO uint32_t u32_cntrdataadc, u32_sumdataadc;
__IO uint16_t u16_adc_callback[2];

maf 	fltr1;
float 	fltr_arr1[16];

typedef enum {
	Battery_0 = 0,
	Battery_10,
	Battery_20,
	Battery_30,
	Battery_40,
	Battery_50,
	Battery_60,
	Battery_70,
	Battery_80,
	Battery_90,
	Battery_100,
	Battery_charging,
	Battery_fail,

} battery_status;
battery_status bttry_sts_now;

typedef enum {
	SM800_MDM_INVLD = -1,	// GSM unLoad
	SM800_SGLV_NNE	= 0,	// NO SIGNAL
	SM800_SGLV_20,			// SIGNAL LV 20%
	SM800_SGLV_40,			// SIGNAL LV 40%
	SM800_SGLV_60,			// SIGNAL LV 60%
	SM800_SGLV_80,			// SIGNAL LV 80%
	SM800_SGLV_100,			// SIGNAL LV 100%

} sm800_sig_sts;
sm800_sig_sts SM800_sigSts_lv;

typedef enum {
	hm_none 	= 0,
	hm_loc_nw	= 1,
	hm_set_td	= 2,
	hm_strg		= 3,
	hm_trck_me	= 4,
	hm_pwe_mgnt	= 5,		// Main Menus

} en_common_menu;
en_common_menu	goto_Home_mn;

struct rmc_frame{
	char	gps_time_utc;		// 1
	char	gps_status;			// 2
	float	gps_latitude;		// 3
	float	gps_longitude;		// 5
	float	gps_speedog;		// 7
	char 	gps_dateddmmyy;		// 9
};
struct rmc_frame	st_getRMCfr;

struct fr_svgpsdt st_frwrGPSdt, st_frrdGPSdt;

struct gps_utc_time{
	int gps_sec;
	int gps_min;
	int gps_hour;
	int gps_date;
	int gps_day;
	int gps_month;
	int gps_year;
};
struct gps_utc_time st_gpsUTCtm, st_rdEpch_EEP;

struct minmea_GSV_sat_info{
	int gps_msg_nr;
	int gps_elevation_sats;
    int azimuth;
    int snr;
};
struct minmea_GSV_sat_info st_get_GSV_sat_info;

struct gps_gsv_frame{
    int gps_total_msgs;
    int gps_msg_nr;
    int gps_total_sats;
    struct minmea_sat_info sats[4];
};
struct gps_gsv_frame st_gps_get_gsv;

struct wr_gpsdt2EEP{
	struct rmc_frame 			get_rmc_data_frame;
	struct gps_utc_time			get_utc_time_date;
};
struct wr_gpsdt2EEP st_wrdt2EEP, st_rddtfrEEP;

struct cfg_EEPgps st_eep;


/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart1_rx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM2_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void Opening_Display();
void Write_GPSeepr_data();
void Get_GPStmdt(uint8_t mode);
void Get_locGPS();
void Header_Menus(uint8_t header_code);
void Battery_Health();
void Dpy_Batt_lv(uint8_t lv_bttry, int Mode);
void SIM800_Sig_lv(uint8_t S800Siglv);
void Pr_Storage_mn();
void Home_mn(uint8_t hmMenu);
void Sub1_mn(uint8_t sb1Menu);
void Trck_me_mn();

maf maf_create(float* elem, const int size)
{
	maf	tmp;

	tmp.size	= size;
	tmp.elem	= elem;
	tmp.total	= 0;
	tmp.avg		= 0;
	tmp.curs	= 0;

	return tmp;
}

void maf_update(maf* maf_ctnr, const float data)
{
	maf_ctnr->total	-= maf_ctnr->elem[maf_ctnr->curs];
	maf_ctnr->total	+= data;
	maf_ctnr->avg	 = (float) maf_ctnr->total
								/ maf_ctnr->size;
	maf_ctnr->elem[maf_ctnr->curs]
					 = data;

	if(maf_ctnr->curs
			< (maf_ctnr->size - 1))
		maf_ctnr->curs++;
	else
		maf_ctnr->curs = 0;
}

void maf_reset(maf* maf_ctnr,const int size){
	maf_ctnr->total=0;
	maf_ctnr->avg	=0;
	maf_ctnr->curs=0;
	memset( maf_ctnr->elem,0,size);
}

uint16_t u16_fl_mapping(uint32_t data_input, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
	return (data_input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float fl_mapping(float data_input, float in_min, float in_max, float out_min, float out_max)
{
#ifdef USE_MAP_FILTER

	float temp_datainput;
	uint16_t temp_rddata;

	maf_update(&fltr1, data_input);
	temp_datainput = fltr1.avg;
	temp_datainput = (temp_datainput - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

	if ( temp_datainput < 0 ){

		temp_datainput = 0;
	}

	temp_datainput *= 100.0;
	//temp_rddata = (temp_datainput > (floor(temp_datainput)+0.5f)) ? ceil(temp_datainput) : floor(temp_datainput);
	temp_rddata = temp_datainput ;
	temp_datainput = temp_rddata;
	temp_datainput /= 100;


	return temp_datainput;

#endif

#ifdef USE_MAP_NORMAL
	return (data_input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
#endif


}

void epoch_to_date(unsigned long epoch){

	struct tm  ts;
	//char       buf[80];
	// Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
	//epoch = 1584604922;
	//epoch = ul_epochnih;

	ts = *(localtime (&epoch));
	//strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
	st_rdEpch_EEP.gps_hour 	= ts.tm_hour;
	st_rdEpch_EEP.gps_min 	= ts.tm_min;
	st_rdEpch_EEP.gps_sec 	= ts.tm_sec;
	st_rdEpch_EEP.gps_day 	= ts.tm_wday;
	st_rdEpch_EEP.gps_date 	= ts.tm_mday;
	st_rdEpch_EEP.gps_month = ts.tm_mon+1;
	st_rdEpch_EEP.gps_year 	= ts.tm_year+1900;

	/*
	 Printf("| Data EPOCH to Date  |  %2d:%02d:%02d %2d.%2d.%4d \r\n",
			st_readEpoch_eepr.hour,
			st_readEpoch_eepr.min,
			st_readEpoch_eepr.sec,
			st_readEpoch_eepr.date,
			st_readEpoch_eepr.month,
			st_readEpoch_eepr.year
			);
	*/
	//Printf("Epoch to Human date : %s\n", buf);

}

unsigned long date_to_epoch(unsigned int year, uint8_t month, uint8_t date, uint8_t hour, uint8_t minute, uint8_t second){

    struct tm t;
    time_t t_of_day;

    t.tm_year = year - 1900; 	// 2019-1900;  // Year - 1900
    t.tm_mon = month - 1;		// Month, where 0 = jan
    t.tm_mday = date;			// Day of the month
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = second;
    t.tm_isdst = -1;			// Is DST on? 1 = yes, 0 = no, -1 = unknown
    t_of_day = mktime(&t);

    //Printf("seconds since the Epoch: %ld\n", (long) t_of_day);
    return t_of_day;
}

int printf0 (const char *fmt, ...)		{
	va_list args;
	int i;
	char printbuffer[512];
	va_start (args, fmt);
	i = vsprintf (printbuffer, fmt, args);
	va_end (args);
	HAL_UART_Transmit(&huart3,(uint8_t *)printbuffer,i,100);
	/* Print the string */
	//if( xSemSer0 != NULL )    {
	//	if( xSemaphoreTake( xSemSer0, ( portTickType ) 10 ) == pdTRUE )	{
	//		vSerialPutString(xPort, printbuffer,strlen(printbuffer));
	//		xSemaphoreGive( xSemSer0 );
	//	}
	//}
	//vTaskDelay(1);
	return 0;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
//  UNUSED(huart1);
	if (huart->Instance == USART1)
	{
		gps_rawdata[idx] =  ch_dpy_bufferGPS[0];
		//Printf("%c",ch_dpy_bufferGPS[0]);
		//gps_rawdata[idx] =  ch_dpy_bufferGPS[0];
		if (flag == 0){								// Nyari $GPRMC
			if (idx == 0 && gps_rawdata[idx] == '$'){
				idx++;
			}
			else if (idx == 1 && gps_rawdata[idx] == 'G'){
				idx++;
			}
			else if (idx == 2 && gps_rawdata[idx] == 'P'){
				idx++;
				flag=1;
			}
			else {
				idx = 0;
				flag = 0;
				memset(gps_rawdata,0,sizeof(gps_rawdata));
			}
		}
		else if ( flag == 1 ){					// Ketemu enter
			if (gps_rawdata[idx] == '\r') {
				flag = 2;
				gps_rawdata[idx] = 0;
				//gps_rawdata[idx-1] = 0;
			}
			else {
				idx++;
			}
		}
		else if ( flag == 2 ) {

			//Printf("~~%s~~ \r\n",gps_rawdata);		// Cek validasi data $GPxxx data masuk
			switch (minmea_sentence_id(gps_rawdata, false)) {
				case MINMEA_SENTENCE_RMC: {
					struct minmea_sentence_rmc frame;
					if (minmea_parse_rmc(&frame, gps_rawdata)) {
						//Printf("\r\n NYANGKUT DI switch RMC BOSS  \r\n");
						gps_validity 					= frame.valid ;
						st_getRMCfr.gps_latitude	= minmea_tocoord(&frame.latitude);
						st_getRMCfr.gps_longitude	= minmea_tocoord(&frame.longitude);
						st_getRMCfr.gps_speedog		= minmea_tofloat(&frame.speed);

						st_gpsUTCtm.gps_sec 	= frame.time.seconds;
						st_gpsUTCtm.gps_min 	= frame.time.minutes;
						st_gpsUTCtm.gps_hour 	= frame.time.hours;
						st_gpsUTCtm.gps_day 	= 0;
						st_gpsUTCtm.gps_date 	= frame.date.day;
						st_gpsUTCtm.gps_month	= frame.date.month;
						st_gpsUTCtm.gps_year 	= frame.date.year+2000;
					}
					else {
						//Printf("$xxRMC sentence is not parsed\n\r");
					}
				} break;

				case MINMEA_SENTENCE_GSV: {
					struct minmea_sentence_gsv frame;
					if (minmea_parse_gsv(&frame, gps_rawdata)) {
						//Printf("\r\n NYANGKUT DI switch GSV BOSS  \r\n");
						for (int i = 0; i < 4; i++){
							//st_gps_get_gsv.gps_total_msgs 		= ;
							st_get_GSV_sat_info.gps_msg_nr 			= frame.sats[i].nr;
							st_get_GSV_sat_info.gps_elevation_sats 	= frame.sats[i].elevation;
							st_get_GSV_sat_info.azimuth 			= frame.sats[i].azimuth;
							st_get_GSV_sat_info.snr 				= frame.sats[i].snr;
						}
					}
					else {
						//printf("$xxGSV sentence is not parsed\n");
					}
				} break;

				default :{		} break;
			}
			idx = 0;
			flag = 0;
			idxgp=0;
			memset(gps_rawdata,0,sizeof(gps_rawdata));
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{

	if(hadc->Instance == ADC1){
		u32_sumdataadc += u16_adc_callback[0];
		//tempadc = u16_adc_callback[0];
		//tempadc *= 3.3;
		//tempadc /= 4096;
		u32_cntrdataadc++;
		//Printf("ADC : %f \r\n", u32_sumdataadc[10]);
		if (u32_cntrdataadc >= 20){
			fl_rawdataadc = (float)u32_sumdataadc/20.0;
			fl_rawtovadc  = fl_rawdataadc / ADC_BUF_LEN;
			fl_rawtovadc *= ADC_VREFF;
			fl_rawtovadc *= VRESS_PLL;
			fl_rawtovadc += 1.24;

			maf_update(&fltr1, fl_rawtovadc);	//fl_rawtovadc

			batt_meter = fl_mapping(fltr1.avg, BATT_MIN_PRC, BATT_MAX_PRC, MIN_PRCENTAGE, MAX_PRCENTAGE);
			batt_meter = ( batt_meter > 100 )? 100 : batt_meter;
			batt_meter = ( batt_meter < 0 )? 0 : batt_meter;

			u16_bttlvsts = u16_fl_mapping(batt_meter, 0, 100, 0, 10);

			u32_cntrdataadc = 0;
			u32_sumdataadc = 0;
		}
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){

	if (htim->Instance == TIM2){
		tick_tim2++;
		if ( tick_tim2 > 1000 ){
			tick_me++;
			tick_tim2 = 0;
		}
	}

}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();

  /* USER CODE BEGIN 2 */

#ifdef POWER_BUTTON_EN
  int count = 0;
  while(1){

	  if ( BTT_PWR_TRUE ) {
		  count++;
	  }
	  else {
		  count = 0;
	  }
	  if ( count == 20 ) {
		  count = 0;
		  POWER_ON_DVC;		// Force Power On Device
		  EN_BTT_MON;		// Force Power On Battery Monitoring
		  break;
	  }
	  else {
		  POWER_OFF_DVC;
	  }
	  HAL_Delay(100);
  }
#endif
  tick_tim2 = 0;
  fltr1 = maf_create(fltr_arr1,10);
  tick_me = 0;
  HAL_UART_Receive_DMA(&huart1, (uint8_t *)ch_dpy_bufferGPS, 1);
  HAL_ADC_Start_DMA(&hadc1,(uint32_t*)u16_adc_callback, 1);
  HAL_TIM_Base_Start_IT(&htim2);

  HAL_Delay(100);	// Delay for sure
  init_SSD_LIBSELECTOR(0,0,0);			// init LCD OLED display
  //ssd1306_Init_adafruit();
  Opening_Display();
  count_pwr_off = 0;
  EN_BTT_MON;

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  Header_Menus(0);

	  display();
	  HAL_Delay(1);

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 64;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 6, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 7, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA7 PA8 PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void Opening_Display(){
	clearDisplay();
	//drawBitmap(7,0,(uint8_t *)image_data_ss,114,49,1);		// LOGO
	drawBitmap(30,5,(uint8_t *)sstextlogo,66,14,1);		// BRAND NAME
	gfx_Puts("GPS Kalkun v0.1", 0, 23, 1, 1, 1);
	display();
	for(int i=0; i<16; i++){
		drawFastHLine(0,31,i*8,1);
		display();
		HAL_Delay(5);
	}
	HAL_GPIO_WritePin (GPIOB,BZZ_MODE,GPIO_PIN_SET);
	LD_STS_ON;
	LD_IDL_ON;
	HAL_Delay(200);
	HAL_GPIO_WritePin(GPIOB,BZZ_MODE,GPIO_PIN_RESET);
	LD_STS_OFF;
	LD_IDL_OFF;
	clearDisplay();
	HAL_Delay(1000);
	clearDisplay();
}

void Header_Menus(uint8_t header_code){

	uint8_t tmp_dpyslct;

//	tmp_dpyslct = ( tick_me < 3 )?  1 : tmp_dpyslct ;
//	tmp_dpyslct = ( tick_me > 3 )?  2 : ( tick_me > 5 )? tick_me = 0 : tmp_dpyslct ;
//	Get_GPStmdt(tmp_dpyslct);

	Get_GPStmdt ( tmp_dpyslct = ( tick_me > 5 )? tick_me = 0 : ( tick_me > 3 )? 2 : ( tick_me < 3 )? 1 : tmp_dpyslct );
	(gps_validity == true)? drawsolBitmap(73,0,(uint8_t *)GPS_vld1,7,7,1) : drawsolBitmap(73,0,(uint8_t *)GPS_vld0,7,7,1);		// GPS Validity true display icon panah fullfill, else panah kosong
	(fl_rawtovadc < BATT_MIN_PRC)? Dpy_Batt_lv(Battery_fail,0) : Dpy_Batt_lv(u16_bttlvsts,0) ;

/*	if ( tick_me < 3 ){
		tmp_dpyslct = 1 ;
	}
	if ( tick_me > 3 ){
		tmp_dpyslct = 2 ;
	  if ( tick_me > 5 ){
		  tick_me = 0;
	  }
	}*/

}

void Get_GPStmdt(uint8_t mode){

	if ( st_gpsUTCtm.gps_sec < 0 ){		// Kalau angka jam ngaco
		if ( mode == 1 ) {
			gfx_Puts("D&T GPS   ", 0, 0, 1, 1, 1);
		}
		if ( mode == 2 ) {
			gfx_Puts("Not Valid!", 0, 0, 1, 1, 1);
		}
	}
	if ( st_gpsUTCtm.gps_sec > 0 ){
		if ( mode == 1 ) {
			sprintf(ch_dpy_buffer1, "%2d:%02d     ",st_gpsUTCtm.gps_hour+7,st_gpsUTCtm.gps_min);
		}
		if ( mode == 2 ) {
			sprintf(ch_dpy_buffer1, "%2d/%2d/%d",st_gpsUTCtm.gps_date,st_gpsUTCtm.gps_month,st_gpsUTCtm.gps_year);
		}
		else {
			sprintf(ch_dpy_buffer1, "%2d:%02d:%02d | %2d/%2d/%d",\
					st_gpsUTCtm.gps_hour+7,\
					st_gpsUTCtm.gps_min, \
					st_gpsUTCtm.gps_sec,\
					st_gpsUTCtm.gps_date,\
					st_gpsUTCtm.gps_month,\
					st_gpsUTCtm.gps_year);
		}
		gfx_Puts(ch_dpy_buffer1, 0, 0, 1, 1, 1);
	}

}

void Get_locGPS(){

	if ( gps_validity == true ){
		sprintf(ch_dpy_buffer1, "Latitude :%8.6f",st_getRMCfr.gps_latitude);
		gfx_Puts(ch_dpy_buffer1, 0, 8, 1, 1, 1);
		sprintf(ch_dpy_buffer1, "Longitude:%9.6f",st_getRMCfr.gps_longitude);
		gfx_Puts(ch_dpy_buffer1, 0, 16, 1, 1, 1);
		sprintf(ch_dpy_buffer1, "SpeedGnd :%0.2f km/h",st_getRMCfr.gps_speedog);
		gfx_Puts(ch_dpy_buffer1, 0, 24, 1, 1, 1);
		//sprintf(ch_dpy_buffer1, "Data Save:%5d Data", st_eep.total_data);
		//gfx_Puts(ch_dpy_buffer1, 0, 48, 1, 1, 1);
		//display();
	}
	else {
		//        123456789012345678901
		gfx_Puts("Latitude : -- None --", 0, 8, 1, 1, 1);
		gfx_Puts("Longitude: -- None --", 0, 16, 1, 1, 1);
		gfx_Puts("SpeedGnd : -- None --", 0, 24, 1, 1, 1);
	}


}

void Dpy_Batt_lv(uint8_t u8_slclvbttry, int mode){

	sprintf(ch_dpy_buffer1, "%3.0f%%", batt_meter);		// kalau mode dari header menu 0 maka tampilkan adc dari reading mikro, lainya dari pembacaan modem
	gfx_Puts(ch_dpy_buffer1, 91, 0, 1, 1, 1);

	if ( u8_slclvbttry == Battery_0)	{
		drawsolBitmap(115,0,(uint8_t *)bat0,12,7,1);
	}	else if ( u8_slclvbttry == Battery_10)	{
		drawsolBitmap(115,0,(uint8_t *)bat10,12,7,1);
	}	else if ( u8_slclvbttry == Battery_20)	{
		drawsolBitmap(115,0,(uint8_t *)bat20,12,7,1);
	}	else if ( u8_slclvbttry == Battery_30)	{
		drawsolBitmap(115,0,(uint8_t *)bat30,12,7,1);
	}	else if ( u8_slclvbttry == Battery_40)	{
		drawsolBitmap(115,0,(uint8_t *)bat40,12,7,1);
	}	else if ( u8_slclvbttry == Battery_50)	{
		drawsolBitmap(115,0,(uint8_t *)bat50,12,7,1);
	}	else if ( u8_slclvbttry == Battery_60)	{
		drawsolBitmap(115,0,(uint8_t *)bat60,12,7,1);
	}	else if ( u8_slclvbttry == Battery_70)	{
		drawsolBitmap(115,0,(uint8_t *)bat70,12,7,1);
	}	else if ( u8_slclvbttry == Battery_80)	{
		drawsolBitmap(115,0,(uint8_t *)bat80,12,7,1);
	}	else if ( u8_slclvbttry == Battery_90)	{
		drawsolBitmap(115,0,(uint8_t *)bat90,12,7,1);
	}	else if ( u8_slclvbttry == Battery_100)	{
		drawsolBitmap(115,0,(uint8_t *)bat100,12,7,1);
	}	else if ( u8_slclvbttry == Battery_charging){
		drawsolBitmap(115,0,(uint8_t *)batchg,12,7,1);
	}	else if ( u8_slclvbttry == Battery_fail)	{
		drawsolBitmap(115,0,(uint8_t *)batt_fail,12,7,1);
	}
}

void Sim800_siglv(int it_siglv){

	if (it_siglv == SM800_MDM_INVLD){
		drawsolBitmap(62,0,(uint8_t *)Signallost,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_NNE){
		drawsolBitmap(62,0,(uint8_t *)noSignal,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_20){
		drawsolBitmap(62,0,(uint8_t *)Signal20,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_40){
		drawsolBitmap(62,0,(uint8_t *)Signal40,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_60){
		drawsolBitmap(62,0,(uint8_t *)Signal60,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_80){
		drawsolBitmap(62,0,(uint8_t *)Signal80,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_100){
		drawsolBitmap(62,0,(uint8_t *)Signal100,10,7,1); HAL_Delay(1);
	}
}

void Battery_Health(){
	//                       123456789012345678901
	sprintf(ch_dpy_buffer1, "Voltage = %3.2fV  ",fl_rawtovadc);
	gfx_Puts(ch_dpy_buffer1, 0, 16, 1, 1, 1);
	sprintf(ch_dpy_buffer1, "Percent = %3.0f%% ",batt_meter);
	gfx_Puts(ch_dpy_buffer1, 0, 24, 1, 1, 1);
	display();
}

void Tracking_me(){
	//st_get_RMC_frame.gps_speedog =35.5;
	// Device Parking
	//gps_validity = true;
	if ( gps_validity == true ){
		if ( st_getRMCfr.gps_speedog <= 10 ){
			if (u16_tc_parking > 180){
				//Write_GPSeepr_data();
				u8_tc_stb_spd = 0;
				u8_tc_lw_spd = 0;
			}
			HAL_Delay(1);
		}
		if ( st_getRMCfr.gps_speedog >10 ){
			if (u8_tc_lw_spd > 20){
				//Write_GPSeepr_data();
				u8_tc_lw_spd = 0;
				u8_tc_stb_spd = 0;
			}
			HAL_Delay(1);
		}
	}
	HAL_GPIO_WritePin(GPIOA,LED_STATUS,GPIO_PIN_RESET);		// Turn off parking LED status
	HAL_Delay(1);
}

void Write_GPSeepr_data(){

	gfx_Puts("Saving...            ", 0, 56, 1, 1, 1);
	display();

	HAL_Delay(10);
	(st_eep.addr_storage < 100)? st_eep.addr_storage = 100: st_eep.addr_storage;
	u16_tmp_eep_wraddr = (st_eep.total_data * sizeof (st_frwrGPSdt)) + EEPR_ADDR_DTSRTG;

	//Simpan data GPS
	ul_epochnih = date_to_epoch(st_gpsUTCtm.gps_year,st_gpsUTCtm.gps_month,st_gpsUTCtm.gps_date,st_gpsUTCtm.gps_hour,st_gpsUTCtm.gps_min,st_gpsUTCtm.gps_sec);
	// Epoch to UTC time setting
	ul_epochnih = ul_epochnih - ( UTC_IND_CLK * SEC_1HOUR );

	st_frwrGPSdt.epoch_timeformat		= ul_epochnih;
	st_frwrGPSdt.fr_sveeprgps_latitude	= st_getRMCfr.gps_latitude;
	st_frwrGPSdt.fr_sveeprgps_longitude	= st_getRMCfr.gps_longitude;
	st_frwrGPSdt.fr_sveeprgps_speedgnd	= st_getRMCfr.gps_speedog;
	//Check_sizeofGPSdata();
#ifdef RUN_DBG_SERIAL
	Printf("Ready to write >>>>>>>>>>>>>>>>\r\n");
	Printf("Time epochnya 		: %d  \r\n",	ul_epochnih);
	Printf("Data latitude 		: %f  \r\n",	st_getRMCfr.gps_latitude);
	Printf("Data longitude		: %f  \r\n", 	st_getRMCfr.gps_longitude);
	Printf("Speed over Ground	: %f  \r\n", 	st_getRMCfr.gps_speedog);
	Printf("sizeof struct : %d \r\n", sizeof (st_frwrGPSdt));
	Printf(">>>>>>>>>>>>>>>>>>>>>>\r\n");
	Printf("\r\n");
#endif

	HAL_Delay(5);
	//HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, u16_tmp_eep_wraddr, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_tulis_data_ke_eeprom , sizeof (st_tulis_data_ke_eeprom), 1000);
	HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, u16_tmp_eep_wraddr, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_frwrGPSdt , sizeof (st_frwrGPSdt), 1000);

	st_eep.total_data +=1;
	// Check kalau data lebih dari 4000 data, reset counter ke 1 lagi (timpa data)
	if (st_eep.total_data > 50000){
		st_eep.total_data = 0;
		st_eep.flag_ctr +=1;
		//        123456789012345678901
		gfx_Puts("     Low Memory!     ", 0, 56, 1, 1, 1);
		display();
	}
	st_eep.addr_storage = u16_tmp_eep_wraddr;

	HAL_Delay(5);
	HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, EEP_ADDR_CONFIG, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_eep, sizeof (st_eep), 1000);
	//        123456789012345678901
	gfx_Puts("Done !!              ", 0, 56, 1, 1, 1);
	display();
	HAL_Delay(50);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
