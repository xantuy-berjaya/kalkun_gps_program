/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <OneWire.h>

#include "ssd1306_adafruit.h"
#include "adafruit_gfx.h"
#include "minmea.h"

#define POWER_ON		GPIO_PIN_0
#define BTT_POWER_OFF	GPIO_PIN_1

#define BUTTON_OK 		GPIO_PIN_10
#define BUTTON_CLC 		GPIO_PIN_11

#define BATT_MON_EN		GPIO_PIN_7

#define DS18B20_PORT	GPIOB
#define DS18B20_PIN		GPIO_PIN_5

//BUTTON STATE READ
#define BTT_ETR_TRUE	HAL_GPIO_ReadPin(GPIOA, BUTTON_OK) == GPIO_PIN_RESET
#define BTT_CLC_TRUE	HAL_GPIO_ReadPin(GPIOA, BUTTON_CLC) == GPIO_PIN_RESET
#define BTT_PWR_TRUE	HAL_GPIO_ReadPin(GPIOB, BTT_POWER_OFF) == GPIO_PIN_SET

//OUTPUT STATE
#define POWER_ON_DVC	HAL_GPIO_WritePin(GPIOB, POWER_ON, GPIO_PIN_SET)
#define POWER_OFF_DVC	HAL_GPIO_WritePin(GPIOB, POWER_ON, GPIO_PIN_RESET)

#define EN_BTT_MON		HAL_GPIO_WritePin(GPIOA, BATT_MON_EN, GPIO_PIN_SET)
#define DS_BTT_MON		HAL_GPIO_WritePin(GPIOA, BATT_MON_EN, GPIO_PIN_RESET)

//GPIOC
#define LED_ERR			GPIO_PIN_8
#define LED_STATUS		GPIO_PIN_9
#define BZZ_MODE		GPIO_PIN_15

#define LD_STS_ON		HAL_GPIO_WritePin(GPIOA, LED_STATUS, GPIO_PIN_SET)
#define LD_STS_OFF		HAL_GPIO_WritePin(GPIOA, LED_STATUS, GPIO_PIN_RESET)

#define LD_ERR_ON		HAL_GPIO_WritePin(GPIOA, LED_ERR, GPIO_PIN_SET)
#define LD_ERR_OFF		HAL_GPIO_WritePin(GPIOA, LED_ERR, GPIO_PIN_RESET)

#define IO_BZZ_ON		HAL_GPIO_WritePin(GPIOB, BZZ_MODE, GPIO_PIN_SET)
#define IO_BZZ_OFF		HAL_GPIO_WritePin(GPIOB, BZZ_MODE, GPIO_PIN_RESET)


//#define ADC_BUF_LEN 	1024
#define ADC_BUF_LEN 	4096
#define ADC_VREFF		3.3
#define VRESS_PLL		2
#define ADC1_CH6MAX		2860
#define ADC1_CH6MIN		3440

#define LIPO_DEATH_RATE	3.55	//1.19	//2262	// 3.62 Volt
#define LION_DEATH_RATE	3.45	//1.19	//2262	// 3.62 Volt
#define BATT_MIN_PRC	3.60	//1.19	//2262	// 3.62 Volt
#define BATT_MAX_PRC	4.1	//1.40	//2568	// 4.21 Volt
#define MAX_PRCENTAGE	100
#define MIN_PRCENTAGE	0

#define UTC_IND_CLK		7					// UTC+7
#define	SEC_DATE		24*3600UL			// Second
#define	SEC_MONTH		24*3600*31UL		// Second
#define	SEC_YEAR		24*3600*31*12UL		// Second
#define	SEC_HOUR		3600				// Second
#define	SEC_MIN			60					// Second
#define	SEC_SEC			1					// Second

//#define RUN_DBG_USBCOMM
//#define	RUN_DBG_SERIAL
#define USE_MAP_FILTER
//#define POWER_ON_EN
//#define POWER_OFF_EN
//#define FAKEGPS
//#define FAKE_DATETIME
//#define DEATH_DVC_FENCE
//#define TRCKING_MODE_RIDING
#define TRCKING_MODE_TIMING


//***** Function ********

char 	ch_dpy_buffer1			[128];
char	gps_rawdata				[128];
char 	ch_dpy_bufferGPS		[2];
char 	compare_gpsdata[10][32];

int 	idx = 0, huart1_idx = 0, idxgp = 0,	flag = 0;

float 	heading = 0;
float 	fl_rawtovadc, fl_finevadc, fl_rawdataadc, batt_meter;	//
float 	tempadc;

float 	Temperature = 0;

unsigned long ul_epochnih;

bool	gps_validity;
bool	Charging_flag;
bool	tick_tim2_val;

uint8_t u8_gpsERdata			[7];
uint8_t u8_gpsEWdata			[7];
uint8_t eeprom_last_addr		[2];

uint8_t	u8_iMenu, u8_sb1Menu, last_hmMenu, last_sb1Menu;

uint8_t	Temp_byte1, Temp_byte2;
uint8_t	Presence = 0;

uint8_t	u8_bttlvsts;
uint8_t	tick_pwroff, count_pwr_off, tmp_min, tmp_2turnOFF;
uint16_t tick_tim2, tcLedSTS, tcLedERR, tcBZZ, tick_me, tick_gps;
uint16_t tcIOLedSTS, tcIOLedERR, tcIOBZZ;
uint16_t tick_lwspeed, tick_medspeed;

uint16_t	u16_tmp_eep_wraddr;

uint16_t TEMP;

__IO uint32_t u32_cntrdataadc, u32_sumdataadc;
__IO uint16_t u16_adc_callback[2];

maf 	fltr1;
float 	fltr_arr1[16];

typedef enum {
	mnNone = 0,
	mnTrackingme,
	mnStorage,
	mnPowermgn,
	mnHomeMax,
} Home_menu;
Home_menu	i_homeMenu;

typedef enum {
	mnsb1None = 0,
	mnsb1Trackingme,
	mnsb1Storage,
	mnsb1Powermgn,
	mnsb1Max,
} Sub1_menu;
Sub1_menu i_sb1Menu;

typedef enum {
	op_back = 0,
	op_save,
	op_formt,
	op_def,
} Sub1_opt;
Sub1_opt i_option;

typedef enum {
	Battery_0 = 0,
	Battery_10,
	Battery_20,
	Battery_30,
	Battery_40,
	Battery_50,
	Battery_60,
	Battery_70,
	Battery_80,
	Battery_90,
	Battery_100,
	Battery_charging,
	Battery_fail,

} battery_status;
battery_status bttry_sts_now;

typedef enum {
	noneIO = 0,				// POLA NYALA	. =ON : _ =OFF
	LDERR_SYS,				//	.___.____
	LDERR_SMTHNG,			//	.________
	LDSTS_SYS_NRML,			//	._.______
	LDSTS_SYS_PROC,			//	._._._.__	= Toggle
	LDSTS_SYS_FAIL,			//	.___.____
	BZZ_MODE1,				//	.__.__.__
	BZZ_MODE2,				//	.._._..._._
	BZZ_MODE3,				//	._._....___
	LDIO_ERR_OFF,
	LDIO_STS_OFF,
	BZZ_IOFF,
} IOcommand;
IOcommand	i_command_IO;

typedef enum {
	SM800_MDM_INVLD = -1,	// GSM unLoad
	SM800_SGLV_NNE	= 0,	// NO SIGNAL
	SM800_SGLV_20,			// SIGNAL LV 20%
	SM800_SGLV_40,			// SIGNAL LV 40%
	SM800_SGLV_60,			// SIGNAL LV 60%
	SM800_SGLV_80,			// SIGNAL LV 80%
	SM800_SGLV_100,			// SIGNAL LV 100%

} sm800_sig_sts;
sm800_sig_sts SM800_sigSts_lv;

struct rmc_frame{
	char	gps_time_utc;		// 1
	char	gps_status;			// 2
	float	gps_latitude;		// 3
	float	gps_longitude;		// 5
	float	gps_speedog;		// 7
	char 	gps_dateddmmyy;		// 9
};
struct rmc_frame	st_getRMCfr;

struct fr_svgpsdt st_frwrGPSdt, st_frrdGPSdt, st_hdrEpch_td;

struct gps_utc_time st_GPStm, st_rdEpch_tm, st_cmptime, st_loctime;

struct minmea_GSV_sat_info{
	int gps_msg_nr;
	int gps_elevation_sats;
    int azimuth;
    int snr;
};
struct minmea_GSV_sat_info st_get_GSV_sat_info;

struct gps_gsv_frame{
    int gps_total_msgs;
    int gps_msg_nr;
    int gps_total_sats;
    struct minmea_sat_info sats[4];
};
struct gps_gsv_frame st_gps_get_gsv;

struct wr_gpsdt2EEP{
	struct rmc_frame 			get_rmc_data_frame;
	struct gps_utc_time			get_utc_time_date;
};
struct wr_gpsdt2EEP st_wrdt2EEP, st_rddtfrEEP;

struct cfg_EEPgps st_eep;


/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart1_rx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM2_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void Opening_Display();
void Write_GPSeepr_data();
void Get_GPStmdt(uint8_t mode);
void Get_locGPS(uint8_t mode);
void Header_Menus(uint8_t header_code);
void i_Menus(uint8_t mnSelect, uint8_t deepmnSelect, uint8_t option);
void Battery_Health();
void Output_Command(uint8_t command);
void Tracking_me();
void menu_Storage(uint8_t cmd_input);
void Dpy_Batt_lv(uint8_t lv_bttry, int Mode);
unsigned long date_to_epoch(unsigned int year, uint8_t month, uint8_t date, uint8_t hour, uint8_t minute, uint8_t second);
void deviceturnoff();
void RW_DS18B20();
void SIM800_Sig_lv(uint8_t S800Siglv);
void Pr_Storage_mn();
void Home_mn(uint8_t hmMenu);
void Sub1_mn(uint8_t sb1Menu);

/*void delay (uint16_t time)
{
    __HAL_TIM_SET_COUNTER(&htim3,0);
    while ((__HAL_TIM_GET_COUNTER(&htim3))<time);
}*/

void Set_Pin_Input(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin){

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);

}

void Set_Pin_Output(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin){

	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);

}

uint8_t DS18B20_Start (void)
{
	uint8_t Response = 0;
	Set_Pin_Output (DS18B20_PORT, DS18B20_PIN);   // set the pin as output
	HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET);  // pull the pin low
	//delay (480);   // delay according to datasheet
	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);    // set the pin as input
	//delay (80);    // delay according to datasheet
	if (!(HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))) Response = 1;    // if the pin is low i.e the presence pulse is detected
	else Response = -1;
	//delay (400); // 480 us delay totally.
	return Response;
}

void DS18B20_Write (uint8_t data)
{
	Set_Pin_Output (DS18B20_PORT, DS18B20_PIN);  // set as output

	for (int i=0; i<8; i++)
	{
		if ((data & (1<<i))!=0)  // if the bit is high
		{
			// write 1
			Set_Pin_Output (DS18B20_PORT, DS18B20_PIN);  // set as output
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET);  // pull the pin LOW
			//delay (1);  // wait for 1 us
			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // set as input
			//delay (60);  // wait for 60 us
		}

		else  // if the bit is low
		{
			// write 0
			Set_Pin_Output (DS18B20_PORT, DS18B20_PIN);
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET);  // pull the pin LOW
			//delay (60);  // wait for 60 us
			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);

		}
	}
}

uint8_t DS18B20_Read (void)
{
	uint8_t value=0;
	Set_Pin_Input (DS18B20_PORT, DS18B20_PIN);

	for (int i=0;i<8;i++)
	{
		Set_Pin_Output (DS18B20_PORT, DS18B20_PIN);   // set as output

		HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, GPIO_PIN_RESET);  // pull the data pin LOW
		//delay (2);  // wait for 2 us

		Set_Pin_Input (DS18B20_PORT, DS18B20_PIN);  // set as input
		if (HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))  // if the pin is HIGH
		{
			value |= 1<<i;  // read = 1
		}
		//delay (60);  // wait for 60 us
	}
	return value;
}

maf maf_create(float* elem, const int size)
{
	maf	tmp;

	tmp.size	= size;
	tmp.elem	= elem;
	tmp.total	= 0;
	tmp.avg		= 0;
	tmp.curs	= 0;

	return tmp;
}

void maf_update(maf* maf_ctnr, const float data)
{
	maf_ctnr->total	-= maf_ctnr->elem[maf_ctnr->curs];
	maf_ctnr->total	+= data;
	maf_ctnr->avg	 = (float) maf_ctnr->total
								/ maf_ctnr->size;
	maf_ctnr->elem[maf_ctnr->curs]
					 = data;

	if(maf_ctnr->curs
			< (maf_ctnr->size - 1))
		maf_ctnr->curs++;
	else
		maf_ctnr->curs = 0;
}

void maf_reset(maf* maf_ctnr,const int size){
	maf_ctnr->total=0;
	maf_ctnr->avg	=0;
	maf_ctnr->curs=0;
	memset( maf_ctnr->elem,0,size);
}

uint16_t u16_fl_mapping(uint32_t data_input, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max)
{
	return (data_input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float fl_mapping(float data_input, float in_min, float in_max, float out_min, float out_max)
{
#ifdef USE_MAP_FILTER

	float temp_datainput;
	uint16_t temp_rddata;

	maf_update(&fltr1, data_input);
	temp_datainput = fltr1.avg;
	temp_datainput = (temp_datainput - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

	if ( temp_datainput < 0 ){

		temp_datainput = 0;
	}

	temp_datainput *= 100.0;
	//temp_rddata = (temp_datainput > (floor(temp_datainput)+0.5f)) ? ceil(temp_datainput) : floor(temp_datainput);
	temp_rddata = temp_datainput ;
	temp_datainput = temp_rddata;
	temp_datainput /= 100;


	return temp_datainput;

#endif

#ifdef USE_MAP_NORMAL
	return (data_input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
#endif

}

float fl2_mapping(float data_input, float in_min, float in_max, float out_min, float out_max)
{
	return (data_input - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

struct gps_utc_time epoch_to_date( struct gps_utc_time tm , int dt_offst, int mn_offst , int yr_offst, int hr_offst, int mi_offst, int sc_offst){

	struct tm  ts;
	unsigned long ul_epochnih = date_to_epoch(tm.gps_year,tm.gps_month,tm.gps_date,tm.gps_hour,tm.gps_min,tm.gps_sec);
	//char       buf[80];
	// Format time, "ddd yyyy-mm-dd hh:mm:ss zzz"
	//epoch = 1584604922;
	//epoch = ul_epochnih;

	ul_epochnih += dt_offst  * SEC_DATE;
	ul_epochnih += mn_offst  * SEC_MONTH;
	ul_epochnih += yr_offst  * SEC_YEAR;
	ul_epochnih += hr_offst  * SEC_HOUR;
	ul_epochnih += mi_offst  * SEC_MIN;
	ul_epochnih += sc_offst  * SEC_SEC;

	struct gps_utc_time tmp;

	ts = *(localtime (&ul_epochnih));
	//strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
	tmp.gps_hour 	= ts.tm_hour;
	tmp.gps_min 	= ts.tm_min;
	tmp.gps_sec 	= ts.tm_sec;
	tmp.gps_day 	= ts.tm_wday;
	tmp.gps_date 	= ts.tm_mday;
	tmp.gps_month 	= ts.tm_mon+1;
	tmp.gps_year 	= ts.tm_year+1900;

	/*
	 Printf("| Data EPOCH to Date  |  %2d:%02d:%02d %2d.%2d.%4d \r\n",
			st_readEpoch_eepr.hour,
			st_readEpoch_eepr.min,
			st_readEpoch_eepr.sec,
			st_readEpoch_eepr.date,
			st_readEpoch_eepr.month,
			st_readEpoch_eepr.year
			);
	*/
	//Printf("Epoch to Human date : %s\n", buf);

	return tmp;

}

unsigned long date_to_epoch(unsigned int year, uint8_t month, uint8_t date, uint8_t hour, uint8_t minute, uint8_t second){

    struct tm t;
    time_t t_of_day;

    t.tm_year = year - 1900; 	// 2019-1900;  // Year - 1900
    t.tm_mon = month - 1;		// Month, where 0 = jan
    t.tm_mday = date;			// Day of the month
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = second;
    t.tm_isdst = -1;			// Is DST on? 1 = yes, 0 = no, -1 = unknown
    t_of_day = mktime(&t);

    //Printf("seconds since the Epoch: %ld\n", (long) t_of_day);
    return t_of_day;
}

int printf0 (const char *fmt, ...)		{
	va_list args;
	int i;
	char printbuffer[512];
	va_start (args, fmt);
	i = vsprintf (printbuffer, fmt, args);
	va_end (args);
	HAL_UART_Transmit(&huart3,(uint8_t *)printbuffer,i,100);
	/* Print the string */
	//if( xSemSer0 != NULL )    {
	//	if( xSemaphoreTake( xSemSer0, ( portTickType ) 10 ) == pdTRUE )	{
	//		vSerialPutString(xPort, printbuffer,strlen(printbuffer));
	//		xSemaphoreGive( xSemSer0 );
	//	}
	//}
	//vTaskDelay(1);
	return 0;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
//  UNUSED(huart1);
	if (huart->Instance == USART1)
	{
		//HAL_GPIO_TogglePin(GPIOA,LED_ERR);
		gps_rawdata[idx] =  ch_dpy_bufferGPS[0];
		//Printf("%c",ch_dpy_bufferGPS[0]);
		//gps_rawdata[idx] =  ch_dpy_bufferGPS[0];
		if (flag == 0){								// Nyari $GPRMC
			if (idx == 0 && gps_rawdata[idx] == '$'){
				idx++;
			}
			else if (idx == 1 && gps_rawdata[idx] == 'G'){
				idx++;
			}
			else if (idx == 2 && gps_rawdata[idx] == 'P'){
				idx++;
				flag=1;
			}
			else {
				idx = 0;
				flag = 0;
				memset(gps_rawdata,0,sizeof(gps_rawdata));
			}
		}
		else if ( flag == 1 ){					// Ketemu enter
			if (gps_rawdata[idx] == '\r') {
				flag = 2;
				gps_rawdata[idx] = 0;
				//gps_rawdata[idx-1] = 0;
			}
			else {
				idx++;
			}
		}
		else if ( flag == 2 ) {

			//Printf("~~%s~~ \r\n",gps_rawdata);		// Cek validasi data $GPxxx data masuk
			switch (minmea_sentence_id(gps_rawdata, false)) {
				case MINMEA_SENTENCE_RMC: {
					struct minmea_sentence_rmc frame;
					if (minmea_parse_rmc(&frame, gps_rawdata)) {
						//Printf("\r\n NYANGKUT DI switch RMC BOSS  \r\n");
						gps_validity 				= frame.valid ;
						st_getRMCfr.gps_latitude	= minmea_tocoord(&frame.latitude);
						st_getRMCfr.gps_longitude	= minmea_tocoord(&frame.longitude);
						st_getRMCfr.gps_speedog		= minmea_tofloat(&frame.speed);

						st_GPStm.gps_sec 	= frame.time.seconds;
						st_GPStm.gps_min 	= frame.time.minutes;
						st_GPStm.gps_hour 	= frame.time.hours;
						st_GPStm.gps_day 	= 0;
						st_GPStm.gps_date 	= frame.date.day;
						st_GPStm.gps_month	= frame.date.month;
						st_GPStm.gps_year 	= frame.date.year+2000;
					}
					else {
						//Printf("$xxRMC sentence is not parsed\n\r");
					}
				} break;

				case MINMEA_SENTENCE_GSV: {
					struct minmea_sentence_gsv frame;
					if (minmea_parse_gsv(&frame, gps_rawdata)) {
						//Printf("\r\n NYANGKUT DI switch GSV BOSS  \r\n");
						for (int i = 0; i < 4; i++){
							//st_gps_get_gsv.gps_total_msgs 		= ;
							st_get_GSV_sat_info.gps_msg_nr 			= frame.sats[i].nr;
							st_get_GSV_sat_info.gps_elevation_sats 	= frame.sats[i].elevation;
							st_get_GSV_sat_info.azimuth 			= frame.sats[i].azimuth;
							st_get_GSV_sat_info.snr 				= frame.sats[i].snr;
						}
					}
					else {
						//printf("$xxGSV sentence is not parsed\n");
					}
				} break;

				default :{		} break;
			}
			idx = 0;
			flag = 0;
			idxgp=0;
			memset(gps_rawdata,0,sizeof(gps_rawdata));
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){

	if(hadc->Instance == ADC1){
		u32_sumdataadc += u16_adc_callback[0];
/*		tempadc = u16_adc_callback[0];
		tempadc *= 3.3;
		tempadc /= 4096;
		tempadc += 0.65;*/

		u32_cntrdataadc++;
		//Printf("ADC : %f \r\n", u32_sumdataadc[10]);
		if (u32_cntrdataadc >= 30){
			fl_rawdataadc = (float)u32_sumdataadc / 30.0;
			fl_rawtovadc  = fl_rawdataadc * ADC_VREFF;
			fl_rawtovadc /= ADC_BUF_LEN;
			fl_rawtovadc += 0.05;		// Offset Transistor Vbe VCC-0.690V
			fl_rawtovadc /= 0.0909;		// Offset Transistor Vbe VCC-0.690V


			maf_update(&fltr1, fl_rawtovadc);	//fl_rawtovadc

			batt_meter = fl_mapping(fltr1.avg, BATT_MIN_PRC, BATT_MAX_PRC, MIN_PRCENTAGE, MAX_PRCENTAGE);
			batt_meter = ( batt_meter > 100 )? 100 : batt_meter;
			batt_meter = ( batt_meter < 0 )? 0 : batt_meter;

			u8_bttlvsts = u16_fl_mapping(batt_meter, 0, 100, 0, 10);

			u32_cntrdataadc = 0;
			u32_sumdataadc = 0;
		}
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){

	if (htim->Instance == TIM2){
		tick_tim2++;
		tcLedSTS++;
		tcLedERR++;
		tcBZZ++;
		if ( tcLedERR > 10 ){
			tcIOLedERR++;
			tcLedERR = 0;
		}
		if ( tcLedSTS > 10 ){
			tcIOLedSTS++;
			tcLedSTS = 0;
		}
		if ( tcBZZ > 10 ){
			tcIOBZZ++;
			tcBZZ = 0;
		}
		if ( tick_tim2 > 1000 ){
			tick_me++;
			tick_gps++;
			tick_pwroff++;
			tick_lwspeed++;
			tick_medspeed++;
			tick_tim2 = 0;
		}

	}

}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();

  /* USER CODE BEGIN 2 */

  DS_BTT_MON;

#ifdef POWER_ON_EN
  int count = 0;
  while(1){

	  if ( BTT_PWR_TRUE ) {
		  count++;
	  }
	  else {
		  count = 0;
	  }
	  if ( count == 20 ) {
		  count = 0;
		  //POWER_ON_DVC;		// Force Power On Device
		  EN_BTT_MON;			// Force Power On Battery Monitoring
		  break;
	  }
	  else {
		  POWER_OFF_DVC;
	  }
	  HAL_Delay(100);
  }
#endif

  POWER_ON_DVC;

  u8_bttlvsts = 0;
  tick_tim2 = 0;
  tick_me = 0;
  tick_pwroff = 0;
  count_pwr_off = 0;
  tick_lwspeed = 0;
  tick_medspeed = 0;
  tmp_min = 0;
  tmp_2turnOFF = 0;
  u8_iMenu = 1;
  u8_sb1Menu = 0;
  i_homeMenu = mnTrackingme;
  i_sb1Menu = mnsb1None;
  i_option = op_back;

  fltr1 = maf_create(fltr_arr1,10);

  HAL_UART_Receive_DMA(&huart1, (uint8_t *)ch_dpy_bufferGPS, 1);
  HAL_ADC_Start_DMA(&hadc1,(uint32_t*)u16_adc_callback, 1);
  HAL_TIM_Base_Start_IT(&htim2);
  //HAL_TIM_Base_Start(&htim3);

  HAL_Delay(100);	// Delay for sure
  init_SSD_LIBSELECTOR(0,0,0);			// init LCD OLED display
  //ssd1306_Init_adafruit();

#ifdef DEATH_DVC_FENCE
  while(1){
	  if ( fl_rawtovadc < LIPO_DEATH_RATE ){
		  gfx_Puts("Need Charge !", 0, 0, 1, 1, 1);
		  Dpy_Batt_lv( Battery_fail , 0 );
		  display();
		  HAL_Delay(2000);
		  POWER_OFF_DVC;
	  }
	  else {
		  break;
	  }
  }
#endif

  Opening_Display();

  //EN_BTT_MON;		// enable monitoring battery

  u16_tmp_eep_wraddr = EEPR_ADDR_DTSRTG;
  HAL_Delay(100);
  int result = HAL_I2C_Mem_Read(&hi2c1, EEPROM1025_ADDR, EEP_ADDR_CONFIG, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_eep, sizeof (st_eep), 0xFFFF);
  HAL_Delay(100);
  if (result != HAL_OK){
	  gfx_Puts(ch_dpy_buffer1, 40, 16, 1, 1, 1);
	  sprintf(ch_dpy_buffer1, "ERROR :%3d  ",result);
	  display();
  }
  else {
	  u16_tmp_eep_wraddr = EEPR_ADDR_DTSRTG + (st_eep.addr_eeprom * sizeof (st_wrdt2EEP));
  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  Header_Menus(0);
	  if ( u8_iMenu == 1 ){
		  i_Menus( i_homeMenu, 1, 1 );
		  u8_iMenu = 2;
	  }
	  if ( u8_iMenu == 2 ){
		  if ( BTT_CLC_TRUE ){
			  i_homeMenu--;
			  i_homeMenu = ( i_homeMenu < mnTrackingme )? mnPowermgn : i_homeMenu;
			  u8_iMenu = 1;
		  }
		  if ( BTT_ETR_TRUE ){
			  i_homeMenu++;
			  i_homeMenu = ( i_homeMenu > mnPowermgn )? mnTrackingme : i_homeMenu;
			  u8_iMenu = 1;
		  }
		  if ( BTT_PWR_TRUE ){
			  i_sb1Menu = ( i_homeMenu == mnTrackingme )? mnsb1Trackingme : i_sb1Menu;
			  i_sb1Menu = ( i_homeMenu == mnStorage )? mnsb1Storage : i_sb1Menu;
			  i_sb1Menu = ( i_homeMenu == mnPowermgn )? mnsb1Powermgn : i_sb1Menu;
			  last_hmMenu = i_homeMenu;
			  i_Menus( mnsb1None, 2, 1 );
			  u8_iMenu = 3;
		  }
	  }

	  if ( u8_iMenu == 3 ){
		  i_Menus( i_sb1Menu, 2, 1 );
		  if ( BTT_ETR_TRUE ){
			  if ( i_sb1Menu == mnsb1Storage ){
				  i_Menus( i_sb1Menu, 2, 2 );
				  u8_iMenu = 3;
			  }
		  }
		  if ( BTT_CLC_TRUE ){
			  i_Menus( mnsb1None, 2, 1 );
			  u8_iMenu = 1;
		  }
	  }
	  //Get_locGPS();
	  //Output_Command(BZZ_MODE2);
	  //Battery_Health();
	  //Tracking_me();

	  /*************** DS18B20 EXAMPLE 2 METHOD *********************/

	  //RW_DS18B20();

	  /******************* DS18B20 END *****************************/

	  display();

#ifdef POWER_OFF_EN
	  if ( BTT_PWR_TRUE ){
		  tick_pwroff++;
		  if ( tick_pwroff > 10 ){
			  deviceturnoff();
		  }
	  }
	  else {
		  tick_pwroff = 0;
	  }
#endif

	  HAL_Delay(100);

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_71CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 64;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 6, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 7, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);

  /*Configure GPIO pins : PA7 PA8 PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PA10 PA11 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void Opening_Display(){
	clearDisplay();
	//drawBitmap(7,0,(uint8_t *)image_data_ss,114,49,1);		// LOGO
	drawBitmap(30,5,(uint8_t *)sstextlogo,66,14,1);		// BRAND NAME
	gfx_Puts("GPS Kalkun v0.1", 0, 23, 1, 1, 1);
	display();
	for(int i=0; i<16; i++){
		drawFastHLine(0,31,i*8,1);
		display();
		HAL_Delay(5);
	}
	IO_BZZ_ON;
	LD_STS_ON;
	LD_ERR_ON;
	HAL_Delay(100);
	IO_BZZ_OFF;
	LD_STS_OFF;
	LD_ERR_OFF;
	clearDisplay();
	HAL_Delay(1000);
	clearDisplay();
}

void Header_Menus(uint8_t header_code){

	uint8_t tmp_dpyslct, tmp_bttry, tmp_ierr;
	if ( tick_me < 3 ){
		tmp_dpyslct = 1;
	}
	if ( tick_me > 3 ){
		tmp_dpyslct = 2;
		if ( tick_me > 6 ){
			tmp_2turnOFF++;
			tick_me = 0;
		}
	}
	Get_GPStmdt( tmp_dpyslct );

	if ( fl_rawtovadc < BATT_MIN_PRC ){
		tmp_bttry = Battery_fail;
		tmp_ierr = 1;
		if ( tmp_2turnOFF == 30 ){		// More than 3 min auto power off
			tmp_2turnOFF = 0;
			//deviceturnoff();
		}
	}
	else {
		tmp_bttry = u8_bttlvsts ;
	}
	Dpy_Batt_lv( tmp_bttry , 0 );

	//Get_locGPS( tmp_dpyslct );		// Sementara
	if ( gps_validity == true ){
		drawsolBitmap(73,0,(uint8_t *)GPS_vld1,7,7,1);
		Output_Command ( LDSTS_SYS_NRML );
		Output_Command ( BZZ_MODE3 );
		LD_ERR_OFF;
		tmp_ierr *= 0;
	}
	else if ( gps_validity == !true || st_GPStm.gps_sec < 0 ){
		drawsolBitmap(73,0,(uint8_t *)GPS_vld0,7,7,1);
		Output_Command ( LDSTS_SYS_NRML );
		Output_Command ( LDERR_SMTHNG );
		tmp_ierr = 1;
		//IO_BZZ_OFF;
	}
	if ( tmp_ierr == 1 ){
		Output_Command ( BZZ_MODE2 );
	}
	else {
		Output_Command ( BZZ_IOFF );
	}
}

void i_Menus( uint8_t mnSelect, uint8_t deepmnSelect, uint8_t option ){

	if ( deepmnSelect == 1 ){

		drawsolBitmap(5,8,(uint8_t *)motor_tracking,25,25,1);
		drawsolBitmap(35,8,(uint8_t *)Storage_rlase,25,25,1);
		drawsolBitmap(65,8,(uint8_t *)Pwr_mngmnt,25,25,2);

		//display();	// Kalau di run blinking menu
		switch (mnSelect){
			case mnNone			: fillRect(0,8,128,31,0); HAL_Delay(1); break;
			case mnTrackingme	: drawsolBitmap(5,8,(uint8_t *)invmotor_tracking,25,25,1);		break;
			case mnStorage		: drawsolBitmap(35,8,(uint8_t *)Storage_invert,25,25,1);		break;
			case mnPowermgn 	: drawsolBitmap(65,8,(uint8_t *)Pwr_mngmnt_invert,25,25,1);	break;
			default 		: fillRect(0,0,128,55,0);
							  gfx_Puts("    ERROR:HOMEND    ", 0, 16, 1, 1, 1);
							  gfx_Puts("               >Back", 0, 24, 1, 1, 1);
							  break;
		}
		//display();
	}

	if ( deepmnSelect == 2 ){

		switch (mnSelect){
			case mnsb1None :
				fillRect(0,8,128,31,0); HAL_Delay(1);
				break;
			case mnsb1Trackingme :
				Tracking_me();
				break;
			case mnsb1Storage :
				menu_Storage(option);
				//        123456789012345678901
				gfx_Puts("Back<        >Format?", 0, 24, 1, 1, 1);
				break;
			case mnsb1Powermgn :
				Battery_Health();
				break;
			default :
				fillRect(0,0,128,55,0);
				gfx_Puts("    ERROR:HOMEND    ", 0, 16, 1, 1, 1);
				gfx_Puts("               >Back", 0, 24, 1, 1, 1);
				break;
		}
	}
	//HAL_Delay(1);
}

void Get_GPStmdt(uint8_t mode){

#ifdef FAKE_DATETIME

	st_GPStm.gps_hour = 23;
	//st_GPStm.gps_min = 53;
	st_GPStm.gps_sec = 1;

#endif

	if ( st_GPStm.gps_sec < 0 ){		// Kalau angka jam ngaco
		if ( mode == 1 ) {
			gfx_Puts("D&T GPS   ", 0, 0, 1, 1, 1);
		}
		else if ( mode == 2 ) {
			gfx_Puts("Not Valid!", 0, 0, 1, 1, 1);
		}
	}
	else if ( st_GPStm.gps_sec > 0 ){

		//--- Time Date GPS to Epoch
		//ul_epochnih = date_to_epoch(st_GPStm.gps_year,st_GPStm.gps_month,st_GPStm.gps_date,st_GPStm.gps_hour,st_GPStm.gps_min,st_GPStm.gps_sec);
		//ul_epochnih += ( UTC_IND_CLK * SEC_1HOUR );	// Epoch to UTC time setting
		//--- Epoch GPS to Time Date
		st_loctime = epoch_to_date( st_GPStm , 0,0,0,7,0,0 );

		if ( mode == 1 ) {
			sprintf(ch_dpy_buffer1, "%2d:%02d     ",st_loctime.gps_hour,st_loctime.gps_min);
		}
		else if ( mode == 2 ) {
			sprintf(ch_dpy_buffer1, "%2d/%2d/%d",st_loctime.gps_date,st_loctime.gps_month,st_loctime.gps_year);
		}
		/*else {
			sprintf(ch_dpy_buffer1, "%2d:%02d:%02d | %2d/%2d/%d",\
					st_GPStm.gps_hour+7,\
					st_GPStm.gps_min, \
					st_GPStm.gps_sec,\
					st_GPStm.gps_date,\
					st_GPStm.gps_month,\
					st_GPStm.gps_year);
		}*/
		gfx_Puts(ch_dpy_buffer1, 0, 0, 1, 1, 1);
	}

}

void Tracking_me(){

#ifdef FAKEGPS
	st_getRMCfr.gps_latitude = 0.00000;
	st_getRMCfr.gps_longitude = 100.00000;
	st_getRMCfr.gps_speedog =5.5;
	gps_validity = true;
#endif

	uint8_t tmp_gpsslct;

	if ( tick_gps < 3 ){
		tmp_gpsslct = 1;
	}
	if ( tick_gps > 3 ){
		tmp_gpsslct = 2;
		if ( tick_gps > 6 ){
			tick_gps = 0;
		}
	}
	Get_locGPS( tmp_gpsslct );

}

void Get_locGPS( uint8_t mode ){

	if ( gps_validity == true ){
		sprintf(ch_dpy_buffer1, "Latitude :%8.6f  ",st_getRMCfr.gps_latitude);
		gfx_Puts(ch_dpy_buffer1, 0, 8, 1, 1, 1);
		sprintf(ch_dpy_buffer1, "Longitude:%9.6f  ",st_getRMCfr.gps_longitude);
		gfx_Puts(ch_dpy_buffer1, 0, 16, 1, 1, 1);
		if ( mode == 1 ){
			sprintf(ch_dpy_buffer1, "Speed    :%0.2fkm/h",st_getRMCfr.gps_speedog);
			//sprintf(ch_dpy_buffer1, "Speed    :%2d", tmp_min);
		}
		else if ( mode == 2 ){
			sprintf(ch_dpy_buffer1, "Storage  :%5d Data ", st_eep.total_data);
			//sprintf(ch_dpy_buffer1, "Speed    :%2d", tmp_min);
		}
		gfx_Puts(ch_dpy_buffer1, 0, 24, 1, 1, 1);

#ifdef TRCKING_MODE_TIMING
		if ( st_cmptime.gps_min != st_GPStm.gps_min ){
			tmp_min++;
			if ( tmp_min > 2 ){
				Write_GPSeepr_data();
				tmp_min = 0;
			}
			st_cmptime.gps_min = st_GPStm.gps_min;
		}
#endif

#ifdef TRCKING_MODE_RIDING
		if ( st_getRMCfr.gps_speedog < 20 ){
			if ( tick_lwspeed > 60){
				Write_GPSeepr_data();
				tick_lwspeed = 0;
				tick_medspeed = 0;
			}
		}
		if ( st_getRMCfr.gps_speedog > 20 ){
			if ( tick_medspeed > 15){
				Write_GPSeepr_data();
				tick_lwspeed = 0;
				tick_medspeed = 0;
			}
		}
#endif
		if ( BTT_ETR_TRUE ){
			Write_GPSeepr_data();
			HAL_Delay(50);
		}
		//display();
	}
	if ( gps_validity == !true ){
		//        				 123456789012345678901
		gfx_Puts("Latitude : -- None --", 0, 8, 1, 1, 1);
		gfx_Puts("Longitude: -- None --", 0, 16, 1, 1, 1);
		if ( mode == 1 ){
			gfx_Puts("Speed    : -- None -- ", 0, 24, 1, 1, 1);
		}
		else if ( mode == 2 ){
			sprintf(ch_dpy_buffer1, "Storage  :%5dData  ", st_eep.total_data);
			gfx_Puts(ch_dpy_buffer1, 0, 24, 1, 1, 1);
		}

		if ( BTT_ETR_TRUE ){
			//        123456789012345678901
			gfx_Puts(" Cannot Save ", 15, 8, 1, 1, 1);
			gfx_Puts(" GPS not Valid ! ", 15, 16, 1, 1, 1);
			display();
			HAL_Delay(1000);
		}

	}
}

void Dpy_Batt_lv(uint8_t u8_slclvbttry, int mode){

	sprintf(ch_dpy_buffer1, "%3.0f%%", batt_meter);		// kalau mode dari header menu 0 maka tampilkan adc dari reading mikro, lainya dari pembacaan modem
	gfx_Puts(ch_dpy_buffer1, 91, 0, 1, 1, 1);

	if ( u8_slclvbttry == Battery_0)	{
		drawsolBitmap(115,0,(uint8_t *)bat0,12,7,1);
	}	else if ( u8_slclvbttry == Battery_10)	{
		drawsolBitmap(115,0,(uint8_t *)bat10,12,7,1);
	}	else if ( u8_slclvbttry == Battery_20)	{
		drawsolBitmap(115,0,(uint8_t *)bat20,12,7,1);
	}	else if ( u8_slclvbttry == Battery_30)	{
		drawsolBitmap(115,0,(uint8_t *)bat30,12,7,1);
	}	else if ( u8_slclvbttry == Battery_40)	{
		drawsolBitmap(115,0,(uint8_t *)bat40,12,7,1);
	}	else if ( u8_slclvbttry == Battery_50)	{
		drawsolBitmap(115,0,(uint8_t *)bat50,12,7,1);
	}	else if ( u8_slclvbttry == Battery_60)	{
		drawsolBitmap(115,0,(uint8_t *)bat60,12,7,1);
	}	else if ( u8_slclvbttry == Battery_70)	{
		drawsolBitmap(115,0,(uint8_t *)bat70,12,7,1);
	}	else if ( u8_slclvbttry == Battery_80)	{
		drawsolBitmap(115,0,(uint8_t *)bat80,12,7,1);
	}	else if ( u8_slclvbttry == Battery_90)	{
		drawsolBitmap(115,0,(uint8_t *)bat90,12,7,1);
	}	else if ( u8_slclvbttry == Battery_100)	{
		drawsolBitmap(115,0,(uint8_t *)bat100,12,7,1);
	}	else if ( u8_slclvbttry == Battery_charging){
		drawsolBitmap(115,0,(uint8_t *)batchg,12,7,1);
	}	else if ( u8_slclvbttry == Battery_fail)	{
		drawsolBitmap(115,0,(uint8_t *)batt_fail,12,7,1);
	}
}

void Sim800_siglv(int it_siglv){

	if (it_siglv == SM800_MDM_INVLD){
		drawsolBitmap(62,0,(uint8_t *)Signallost,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_NNE){
		drawsolBitmap(62,0,(uint8_t *)noSignal,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_20){
		drawsolBitmap(62,0,(uint8_t *)Signal20,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_40){
		drawsolBitmap(62,0,(uint8_t *)Signal40,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_60){
		drawsolBitmap(62,0,(uint8_t *)Signal60,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_80){
		drawsolBitmap(62,0,(uint8_t *)Signal80,10,7,1); HAL_Delay(1);
	}	else if (it_siglv == SM800_SGLV_100){
		drawsolBitmap(62,0,(uint8_t *)Signal100,10,7,1); HAL_Delay(1);
	}
}

void Battery_Health(){
	//                       123456789012345678901
	//sprintf(ch_dpy_buffer1, "Voltage = %3.2fV  ", tempadc);
	//printf(ch_dpy_buffer1, "Rwadc = %3.2fV  ",fl_rawdataadc);
	sprintf(ch_dpy_buffer1, "Voltage = %3.2fV /%3.0f%%",fl_rawtovadc, batt_meter);
	gfx_Puts(ch_dpy_buffer1, 0, 8, 1, 1, 1);
	//sprintf(ch_dpy_buffer1, "%% = %3.0f%% ",batt_meter);
	//gfx_Puts(ch_dpy_buffer1, 0, 16, 1, 1, 1);
	//sprintf(ch_dpy_buffer1, "rw2adc = %3.0f%% ", fl_rawtovadc);
	//gfx_Puts(ch_dpy_buffer1, 0, 24, 1, 1, 1);
	display();
}

void menu_Storage(uint8_t cmd_input){

	uint16_t data_limit = 800;	// data = 50000data/800kb
	uint16_t tmp_kb_szdata = (uint16_t)(st_eep.addr_eeprom * sizeof (st_frwrGPSdt))/1000;
	uint16_t tmp_left_kb_szdata = data_limit - tmp_kb_szdata;
	uint16_t percent_data = u16_fl_mapping(tmp_kb_szdata,0,data_limit,0,100);

	if (cmd_input == 1){
		sprintf(ch_dpy_buffer1, "Data :%5d/ %3d%%",st_eep.total_data, percent_data);
		gfx_Puts(ch_dpy_buffer1, 0, 8, 1, 1, 1);
		sprintf(ch_dpy_buffer1, "Usage:%3d/ %3dKb",tmp_kb_szdata, tmp_left_kb_szdata);
		gfx_Puts(ch_dpy_buffer1, 0, 16, 1, 1, 1);;
		//display();
	}
	if (cmd_input == 2){
		//        123456789012345678901
		gfx_Puts("     Formating...    ", 0, 24, 1, 1, 1);
		display();
		st_eep.total_data = 0;
		st_eep.addr_eeprom = 0;
		HAL_Delay(750);
		gfx_Puts("     Done !      ", 0, 24, 1, 1, 1);
		display();
		HAL_Delay(250);
	}

}

void Write_GPSeepr_data(){

	gfx_Puts(" Saving... ", 25, 16, 1, 1, 1);
	display();

	HAL_Delay(10);
	(st_eep.addr_eeprom < 100)? st_eep.addr_eeprom = 100: st_eep.addr_eeprom;
	u16_tmp_eep_wraddr = (st_eep.total_data * sizeof (st_frwrGPSdt)) + EEPR_ADDR_DTSRTG;

	//Simpan data GPS
	ul_epochnih = date_to_epoch(st_GPStm.gps_year,st_GPStm.gps_month,st_GPStm.gps_date,st_GPStm.gps_hour,st_GPStm.gps_min,st_GPStm.gps_sec);
	// Epoch to UTC time setting
	ul_epochnih = ul_epochnih - ( UTC_IND_CLK * SEC_HOUR );

	st_frwrGPSdt.epoch_timeformat		= ul_epochnih;
	st_frwrGPSdt.fr_sveeprgps_latitude	= st_getRMCfr.gps_latitude;
	st_frwrGPSdt.fr_sveeprgps_longitude	= st_getRMCfr.gps_longitude;
	st_frwrGPSdt.fr_sveeprgps_speedgnd	= st_getRMCfr.gps_speedog;
	HAL_Delay(5);
	//----- write lat, long, speed, data
	//HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, u16_tmp_eep_wraddr, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_wrdt2EEP , sizeof (st_wrdt2EEP), 1000);
	HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, u16_tmp_eep_wraddr, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_frwrGPSdt , sizeof (st_frwrGPSdt), 1000);

	st_eep.total_data +=1;
	// Check kalau data lebih dari 4000 data, reset counter ke 1 lagi (timpa data)
	if (st_eep.total_data > 50000){
		st_eep.total_data = 0;
		st_eep.flag_ctr +=1;
		//        123456789012345678901
		gfx_Puts(" WARNING!      ", 15, 8, 1, 1, 1);
		gfx_Puts(" Memory Limit! ", 15, 16, 1, 1, 1);
		display();
		HAL_Delay(1000);
	}
	st_eep.addr_eeprom = u16_tmp_eep_wraddr;
	HAL_Delay(5);
	//---- write last address eeprom to config
	HAL_I2C_Mem_Write(&hi2c1, EEPROM1025_ADDR, EEP_ADDR_CONFIG, I2C_MEMADD_SIZE_16BIT, (uint8_t*) &st_eep, sizeof (st_eep), 1000);
	//        123456789012345678901
	gfx_Puts(" Done!   ", 25, 16, 1, 1, 1);
	display();
	HAL_Delay(100);
}

void Output_Command(uint8_t command){
/*
 * 	noneIO = 0,				// POLA NYALA	. =ON : _ =OFF
	LDERR_SYS,				//	.___.____
	LDERR_SMTHNG,			//	.________
	LDSTS_SYS_NRML,			//	._.______
	LDSTS_SYS_PROC,			//	._._._.__	= Toggle
	LDSTS_SYS_FAIL,			//	.___.____
	BZZ_MODE1,				//	.__.__.__
	BZZ_MODE2,				//	.._._..._._
	BZZ_MODE3,				//	._._....___
 */
	if ( tcIOLedSTS > 250 ){
		tcIOLedSTS = 0;
	}
	if ( tcIOLedERR > 250 ){
		tcIOLedERR = 0;
	}
	if ( tcIOBZZ > 2000 ){
		tcIOBZZ = 0;
	}

	switch ( command ){
	case noneIO :
		LD_ERR_OFF;
		LD_STS_OFF;
		IO_BZZ_OFF;
		break;

	case  LDERR_SYS :
		if ( tcIOLedERR > 10 ){
			LD_ERR_OFF;
		}
		if ( tcIOLedERR < 10 ){
			LD_ERR_ON;	}

		break;
	case LDERR_SMTHNG :

		if ( tcIOLedERR < 10 ){
			LD_ERR_ON;
		}
		if ( tcIOLedERR > 10 && tcIOLedERR < 20 ){
			LD_ERR_OFF;
		}
		if ( tcIOLedERR > 20 && tcIOLedERR < 30 ){
			LD_ERR_ON;
		}
		if ( tcIOLedERR > 30 && tcIOLedERR < 40){
			LD_ERR_OFF;
		}
		if ( tcIOLedERR > 50 && tcIOLedERR < 60){
			LD_ERR_ON;
		}
		if ( tcIOLedERR > 60){
			LD_ERR_OFF;
		}
		break;

	case LDSTS_SYS_NRML :
		if ( tcIOLedSTS < 5 ){
			LD_STS_ON;
		}
		if ( tcIOLedSTS > 5 ){
			LD_STS_OFF;
		}
		break;

	case LDSTS_SYS_FAIL :
		if ( tcIOLedSTS < 5 ){
			LD_STS_ON;
		}
		if ( tcIOLedSTS > 5 && tcIOLedSTS < 80 ){
			LD_STS_OFF;
		}
		if ( tcIOLedSTS > 80 && tcIOLedSTS < 85 ){
			LD_STS_ON;
		}
		if ( tcIOLedSTS > 85 ){
			LD_STS_OFF;
		}
		break;

	case BZZ_MODE1 :	// ._._...__
		if ( tcIOBZZ < 20 ){
			IO_BZZ_ON;	}
		if ( tcIOBZZ > 20 ){
			IO_BZZ_OFF;
		}
		break;

	case BZZ_MODE2 :	// ._._...__

		if ( tcIOBZZ < 50 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 50 && tcIOBZZ < 100 ){
			IO_BZZ_OFF;
		}
		if ( tcIOBZZ > 100 && tcIOBZZ < 150 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 150 ){
			IO_BZZ_OFF;
		}

		break;

	case  BZZ_MODE3 :
		if ( tcIOBZZ < 50 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 50 && tcIOBZZ < 80 ){
			IO_BZZ_OFF;
		}
		if ( tcIOBZZ > 80 && tcIOBZZ < 130 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 130 && tcIOBZZ < 160 ){
			IO_BZZ_OFF;
		}
		if ( tcIOBZZ > 160 && tcIOBZZ < 210 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 210 && tcIOBZZ < 240 ){
			IO_BZZ_OFF;
		}
		if ( tcIOBZZ > 240 && tcIOBZZ < 270 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 270 && tcIOBZZ < 300 ){
			IO_BZZ_OFF;
		}
		if ( tcIOBZZ > 300 && tcIOBZZ < 330 ){
			IO_BZZ_ON;
		}
		if ( tcIOBZZ > 330 ){
			IO_BZZ_OFF;
		}

		break;

	case LDIO_STS_OFF :
		LD_STS_OFF;
		break;
	case LDIO_ERR_OFF :
		LD_ERR_OFF;
		break;
	case BZZ_IOFF :
		IO_BZZ_OFF;
		break;
	default :
		LD_ERR_OFF;
		LD_STS_OFF;
		tcIOLedERR = 0;
		tcIOLedSTS = 0;
		tcIOBZZ = 0;
		break;
	}
}

void deviceturnoff(){

	//          123456789012345678901
	gfx_Puts("                     ", 0, 8, 1, 1, 1);
	gfx_Puts("   -- POWER OFF --   ", 0, 16, 1, 1, 1);
	gfx_Puts("                     ", 0, 24, 1, 1, 1);
	display();
	HAL_Delay(2000);
	POWER_OFF_DVC;
}

void RW_DS18B20(){

	Presence = DS18B20_Start();
	HAL_Delay (1);
	DS18B20_Write (0xCC);  // skip ROM
	DS18B20_Write (0x44);  // convert t
	HAL_Delay (500);

	Presence = DS18B20_Start();
	HAL_Delay(1);
	DS18B20_Write (0xCC);  // skip ROM
	DS18B20_Write (0xBE);  // Read Scratch-pad

	Temp_byte1 = DS18B20_Read();
	Temp_byte2 = DS18B20_Read();

	TEMP = ( Temp_byte2<<8 )|Temp_byte1;
	Temperature = (float)TEMP/16;

	sprintf(ch_dpy_buffer1, "Temp = %5.3f ",Temperature );
	gfx_Puts(ch_dpy_buffer1, 0, 8, 1, 1, 1);

}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
