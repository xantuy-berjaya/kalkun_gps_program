/*
 * adafruit_gfx.h
 *
 *  Created on: Dec 19, 2018
 *      Author: Rahadi
 */

#ifndef INC_ADAFRUIT_ADAFRUIT_GFX_H_
#define INC_ADAFRUIT_ADAFRUIT_GFX_H_

#include "stdint.h"
#include <stdbool.h>
//#include "stm32l0xx_hal.h"
#include "gfxfont.h"


#ifndef pgm_read_byte
 #define pgm_read_byte(addr) (*(const unsigned char *)(addr))
#endif
#ifndef pgm_read_word
 #define pgm_read_word(addr) (*(const unsigned short *)(addr))
#endif
#ifndef pgm_read_dword
 #define pgm_read_dword(addr) (*(const unsigned long *)(addr))
#endif

// Pointers are a peculiar case...typically 16-bit on AVR boards,
// 32 bits elsewhere.  Try to accommodate both...

#if !defined(__INT_MAX__) || (__INT_MAX__ > 0xFFFF)
 #define pgm_read_pointer(addr) ((void *)pgm_read_dword(addr))
#else
 #define pgm_read_pointer(addr) ((void *)pgm_read_word(addr))
#endif

#define swap(a, b) { int16_t t = a; a = b; b = t; }

int16_t _width, _height, // Display w/h as modified by current rotation
  	  cursor_x, cursor_y;
uint16_t
   textcolor, textbgcolor;
uint8_t
   textsize,
   rotation, _cp437;         ///< If set, use correct CP437 charset (default is off)
GFXfont
  *gfxFont;       ///< Pointer to special font
bool
   wrap; // If set, 'wrap' text at right edge of display


 // This MUST be defined by the subclass:
//void drawPixel(int16_t x, int16_t y, uint16_t color) ;

void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void fillScreen(uint16_t color);
void invertDisplay(bool i);

 // These exist only with Adafruit_GFX (no subclass overrides)

void drawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
void drawCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername,
uint16_t color);
void fillCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);
void fillCircleHelper(int16_t x0, int16_t y0, int16_t r, uint8_t cornername,
int16_t delta, uint16_t color);
void drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
int16_t x2, int16_t y2, uint16_t color);
void fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
int16_t x2, int16_t y2, uint16_t color);
void drawRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h,
int16_t radius, uint16_t color);
void fillRoundRect(int16_t x0, int16_t y0, int16_t w, int16_t h,
int16_t radius, uint16_t color);
void drawBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
void drawsolBitmap(int16_t x, int16_t y, const uint8_t bitmap[],int16_t w, int16_t h, uint16_t color);
void drawsolrotBitmap(int16_t x, int16_t y, const uint8_t bitmap[],int16_t w, int16_t h, uint16_t color);
void drawBitmap2(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color, uint16_t bg);
void drawXBitmap(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color);
void drawChar(int16_t x, int16_t y, unsigned char c, uint16_t color,uint16_t bg, uint8_t size);
void setCursor(int16_t x, int16_t y);
void setTextColor(uint16_t c);
void setTextColor2(uint16_t c, uint16_t bg) ;
void setTextSize(uint8_t s);
void setTextWrap(bool w);
void writeFastVLine(int16_t x, int16_t y,
        int16_t h, uint16_t color);
void writeFastHLine(int16_t x, int16_t y,
        int16_t w, uint16_t color);

void cp437(bool x);
void setFont(const GFXfont *f);
void getTextBounds(const char * str, int16_t x, int16_t y,int16_t *x1, int16_t *y1, uint16_t *w, uint16_t *h);
//void getTextBounds(const char *string, int16_t x, int16_t y,int16_t *x1, int16_t *y1, uint16_t *w, uint16_t *h);
//void getTextBounds(const char*str, uint16_t x, uint16_t y,uint16_t *x1, uint16_t *y1, uint16_t *w, uint16_t *h);

// Additional of me
char gfx_Puts(const char *str, int16_t x, int16_t y, uint16_t color, uint16_t bg, uint8_t size);
void getTextBounds2(const char *str, int16_t x, int16_t y, int16_t *x1, int16_t *y1, uint16_t *w, uint16_t *h);

void setRotation(uint8_t r);
int   write(uint8_t);
int16_t height(void) ;
int16_t width(void) ;

uint8_t getRotation(void) ;
void lcd_puts(const char *str);

#endif /* INC_ADAFRUIT_ADAFRUIT_GFX_H_ */
